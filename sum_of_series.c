#include <stdio.h> 
#include <math.h>
int input();
int compute(int);
void output(int,int);
int main()
{
    int n,sum;
    n=input();
    sum=compute(n);
    output(sum,n);
}
int input()
{
    int n;
    printf("Enter the value of n till which the series should be printed: ");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int sum=0;
   for(int i=1;i<=n;i++)
    {
       sum=sum+pow(i,2);
   } 
   return sum;
}
void output(int sum,int n)
{
    for(int i=1;i<=n;i++)
        {
        printf("%d^2+",i);
    }
    printf("= %d",sum);
}