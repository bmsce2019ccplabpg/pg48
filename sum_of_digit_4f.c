#include <stdio.h>
int input();
int compute(int);
void output(int);
int main()
{
    int n;
    n=input();
    int sum;
    sum=compute(n);
    output(sum);
    return 0;
}
int input()
{
    int n;
    printf("Enter the number");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int sum=0;
	while(n!=0)
	{
		int rem=n%10;
		sum=sum+rem;
		n=n/10;
	}
    return sum;
}
void output(int sum)
{
    printf("The sum of digits of the entered number=%d",sum);
}