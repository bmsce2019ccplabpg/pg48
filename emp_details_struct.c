#include <stdio.h>
int main()
{
    struct employee
    {
        char empname[10];
        int empid;
        char doj[10];
        float salary;
        char designation[20];
    }s;
    printf("Enter employee details");
    printf("\nEmployee Name= ");
    scanf("%s",s.empname);
    printf("\nEmployee ID= ");
    scanf("%d",&s.empid);
    printf("\nDate of joining= ");
    scanf("%s",s.doj);
    printf("\nSalary= ");
    scanf("%f",&s.salary);
    printf("\nDesignation= ");
    scanf("%s",s.designation);
    printf("\nEmployee details are as follows");
    printf("\nName= %s",s.empname);
    printf("\nEmployee ID= %d ",s.empid);
    printf("\nDate of joining= %s ",s.doj);
    printf("\nSalary= %f ",s.salary);
    printf("\nDesignation= %s ",s.designation);
    return 0;
}