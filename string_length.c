#include <stdio.h> 
void input(char[]);
int compute(char[]);
void output(int);
int main()
{
    char s[100];
    input(s);
    int len;
    len=compute(s);
    output(len);
    return 0;
}
void input(char s[])
{
    printf("Enter the string");
    gets(s);
}
int compute(char s[])
{
    int i=0;
    while(s[i]!='\0')
    {
       i++; 
    }
    return i;
}
void output(int len)
{
    printf("The length of given string is %d",len);
}