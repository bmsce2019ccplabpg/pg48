 #include <stdio.h>
 int using_ternary(int,int,int);
 void output(int);
 
 int main()
 {
     int a,b,c;
     int res;
     printf("Enter three numbers=");
     scanf("%d %d %d",&a,&b,&c);
     res=using_ternary(a,b,c);
     output(res);
     return 0;
 }
 int using_ternary(int a,int b,int c)
 {
    int res=a>=b?(a>=c ? a:c):(b>=c?b:c);
    return res; 
     
 }
void output(int res)
{
    printf("largest of three numbers=%d",res);
}