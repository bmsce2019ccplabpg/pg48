#include <stdio.h> 
int input();
float compute(int);
void display(float,int);
int main()
{
    int num;
	float sum;
    num=input();
    sum=compute(num);
    display(sum,num);
    return 0;
}
int input()
{
    int num;
    printf("Enter the number");
    scanf("%d",&num);
    return num;
}
float compute(int num)
{
	int fact=1;
	float sum=0.0;
    for(int i=1;i<=num;i++)
	{
		fact=1;
		for(int j=1;j<=i;j++)
		{
			fact=fact*j;
		}
		sum=sum+(float)i/fact;
	}
	return sum;
}
void display(float sum,int num)
{
	for(int i=1;i<=num;i++)
	{
		printf("%d/%d!+ ",i,i);
	}
	printf("= %f",sum);
    
}
    
          