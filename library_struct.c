#include <stdio.h>
int main()
{
    struct library
    {
        char name[20];
        int no;
        struct doi
        {
            int dd;
            int mm;
            int yy;
        }d;
        char author[20];
    }s;
    
    printf("Book name=");
    scanf("%s",s.name);
    printf("Number of copies=");
    scanf("%d",&s.no);
    printf("Date of Issue as dd/mm/yy=");
    scanf("%d %d %d",&s.d.dd,&s.d.mm,&s.d.yy);
    printf("Name of author=");
    scanf("%s",s.author);
    
    printf("\nThe details of book are ");
    printf("\nBook name=%s",s.name);
    printf("\nNumber of copies=%d",s.no);
    printf("\nDate of Issue as dd/mm/yy=%d/%d/%d",s.d.dd,s.d.mm,s.d.yy);
    printf("\nName of author=%s",s.author);
    return 0;
}