#include <stdio.h>
int input1();
void input2(int[],int);
void output(int[],int);
int main()
{
    int n;
    n=input1();
    int a[n];
    input2(a,n);
    output(a,n);
    return 0;
}
int input1()
{
    int n;
    printf("Enter the number of elements in an array");
    scanf("%d",&n);
    return n;
}
void input2(int a[],int n)
{
    int num;
    for(int i=0;i<n;i++)
    {
        printf("Enter a number: ");
        scanf("%d",&num);
        a[i]=num;
    }
}
void output(int a[],int n)
{
    printf("The elements of array are:\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\n",a[i]);
    }
}