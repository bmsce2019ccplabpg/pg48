#include <stdio.h>
int input1();
int input2();
int add(int,int);
void output(int);
int main()
{
    int a,b,res;
    a=input1();
    b=input2();
    res=add(a,b);
    output(res);
    return 0;
    
}
int input1()
{
    int a;
    printf("Enter a number");
    scanf("%d",&a);
    return a;
}
int input2()
{
    int b;
    printf("Enter another number");
    scanf("%d",&b);
    return b;
}
int add(int a,int b)
{
    return a+b;
}
void output(int res)
{
    printf("Addition of two numbers=%d",res);
}