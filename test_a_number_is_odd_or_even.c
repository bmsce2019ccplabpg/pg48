#include <stdio.h> 
int input();
int check(int);
void output(int);
int main()
{
    int num;
	int val;
	num=input();
	val=check(num);
	output(val);
	return 0;
	
}
int input()
{
	int num;
	printf("Enter a number=");
	scanf("%d",&num);
	return num;
}
int check(int num)
{
	if(num%2==0)
	{
		return 1;
	}
	else 
		return 0;
}
void output(int val)
{
	if(val==1)
	{
		printf("The given number is an even number\n");
	}
	else
	{
		printf("The given number is an odd number\n");
	}
}