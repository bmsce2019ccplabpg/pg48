#include <stdio.h> 
#include <math.h>
int input();
int compute(int);
void output(float);
int main()
{
    int n;
    int sum;
    n=input();
    sum=compute(n);
    float avg=(float)sum/n;
    output(avg);
    return 0;
}
int input()
{
    int n;
    printf("Enter the value of n: ");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int sum=0;
    for(int i=1;i<=n;i++)
    {
        sum=sum+i;
    }
    return sum;
}
void output(float avg)
{
    printf("The average of n numbers is %f",avg);
}