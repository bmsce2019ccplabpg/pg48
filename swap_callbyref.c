#include <stdio.h>
int input();
void swap(int*,int*);
void output(int*,int*);
int main()
{
    int a,b;
    a=input();
    b=input();
    swap(&a,&b);
    output(&a,&b);
    return 0;
}
int input()
{
    int z;
    printf("Enter a number= ");
    scanf("%d",&z);
    return z;
}
void swap(int *c,int *d)
{
    int t;
    t=*c;
    *c=*d;
    *d=t;
}
void output(int *a,int *b)
{
    printf("The interchanged numbers are %d and %d ",*a,*b);
}



