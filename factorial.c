#include <stdio.h> 
int input();
int compute(int);
void output(int,int);
int main()
{
    int n;
    int fact;
    n=input();
    fact=compute(n);
    output(fact,n);
    return 0;
}
int input()
{
    int n;
    printf("Enter the value of number: ");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int fact=1;
    for(int i=1;i<=n;i++)
    {
        fact=fact*i;
    }
    return fact;
}
void output(int fact,int n)
{
    printf("The factorial of %d  is %d",n,fact);
}