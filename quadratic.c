#include <stdio.h>
#include <math.h>
void quardratic_roots(int a, int b, int c);
int main ()
{

    float a, b, c;
    printf("Enter the coefficients of x^2, x and constant:");
    scanf ("%f%f%f", &a, &b, &c);
    quardratic_roots(a, b, c);
    return 0;
}

void quardratic_roots(int a, int b, int c)
{
    float D, root1, root2, img2;
    int value,img1;
  
    D = (b * b - 4 * a * c);
  
    if (D == 0)
    {
       value = 2;  
    }
    
    else if (D > 0)
    {
        value=1;
    }
    else
    {
        value=3;
    }


  switch (value)
    {

    case 1:

      {

	    printf ("the roots are real and distinct\n");

	    root1 = ((-b + pow (D, 0.5)) / 2 * a);

	    root2 = ((-b - pow (D, 0.5)) / 2 * a);

	    printf ("The roots are %f and %f", root1, root2);

      }
      break;


    case 2:

      {

	    printf("The roots are real and equal \n");

	    root1 = (-b / (2 * a));

	    root2 = root1;

	    printf("the roots are %f and %f", root1, root2);

      }
      break;


    case 3:

      {

	    D = -D;

	    printf ("the roots are imaginary\n");

	    img1 = (-b / (2 * a));

	    img2= (pow (D, .5) / (2 * a));

	    printf ("the roots are %d+i%f and %d-i%f", img1, img2, img1, img2);

      }
      break;

    default:

      printf ("Entered invalid coefficients,try again");
      break;

    }

}
