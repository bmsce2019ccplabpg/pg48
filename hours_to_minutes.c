#include <stdio.h>
int input1();
int input2();
int time(int,int);
void output(int);
int main()
{
    int hours,min,time_in_min;
    hours=input1();
    min=input2();
    time_in_min=time(hours,min);
    output(time_in_min);
	return 0;
}
int input1()
{
    int hours;
    printf("Enter the number of hours= ");
    scanf("%d",&hours);
    return hours;
}
int input2()
{
    int min;
    printf("Enter the number of minutes= ");
    scanf("%d",&min);
    return min;
}
int time(int hours,int min)
{
    int time_in_min=(hours*60)+min;
    return time_in_min;
}
void output(int time_in_min)
{
     printf("The total time in minutes= %d",time_in_min);
}
    