#include <stdio.h>
int input1();
void input2(int[],int);
int compute(int[],int);
void output(int[],int,int);
int main()
{
    int n;
    n=input1();
    int a[n];
    input2(a,n);
    int small;
    small=compute(a,n);
    output(a,n,small);
    return 0;
}
int input1()
{
    int n;
    printf("Enter the number of elements in an array");
    scanf("%d",&n);
    return n;
}
void input2(int a[],int n)
{
    int num;
    for(int i=0;i<n;i++)
    {
        printf("Enter a number: ");
        scanf("%d",&num);
        a[i]=num;
    }
}
int compute(int a[],int n)
{
    int small=a[0];
    int sp=0;
    for(int i=1;i<n;i++)
    {
        if(a[i]<=small)
        {
            small=a[i];
            sp=i;
        }
    }
    return small;
}
void output(int a[],int n,int small)
{
    printf("The smallest element of array is=%d",small); 
}
