#include <stdio.h>
int input1();
void input2(int[],int);
int compute(int[],int);
void output(int[],int);
int main()
{
    int n;
    n=input1();
    int a[n];
    input2(a,n);
    n=compute(a,n);
    output(a,n);
    return 0;
}
int input1()
{
    int n;
    printf("Enter the number of elements in an array");
    scanf("%d",&n);
    return n;
}
void input2(int a[],int n)
{
    int num;
    for(int i=0;i<n;i++)
    {
        printf("Enter a number: ");
        scanf("%d",&num);
        a[i]=num;
    }
}
int compute(int a[],int n)
{
    int pos;
    printf("Enter the position at which the element is to be deleted: ");
    scanf("%d",&pos);
    for(int i=pos;i<n;i++)
    {
        a[i-1]=a[i];
    }
    n--;
    return n;
}
void output(int a[],int n)
{
     printf("The changed elements of array are:\n");
     for(int i=0;i<n;i++)
     {
         printf("%d\n",a[i]);
     }  
}
