#include <stdio.h>
int main()
{
    struct student
    {
        int rno;
        char name[20];
        struct dob
       {
            int dd;
            int mm;
            int yy;
       }d;
       float fees;
       float cgpa;
    }s1,s2;
    printf("enter the student name\n");
    scanf("%s",s1.name);
    printf("enter the student id\n");
    scanf("%d",&s1.rno);
    printf("enter the date of birth of the student\n");
    scanf("%d%d%d",&s1.d.dd,&s1.d.mm,&s1.d.yy);
    printf("enter the CGPA of the student\n");
    scanf("%f",&s1.cgpa);
    printf("enter the fees of the student\n");
    scanf("%f",&s1.fees);
    printf("enter the student id\n");
    scanf("%d",&s2.rno);
    printf("enter the student name\n");
    scanf("%s",s2.name);
    printf("enter the date of birth of the student\n");
    scanf("%d%d%d",&s2.d.dd,&s2.d.mm,&s2.d.yy);
    printf("enter the CGPA of the student\n");
    scanf("%f",&s2.cgpa);
    printf("enter the fees of the student\n");
    scanf("%f",&s2.fees);
    printf("\n\ndetails of the student 1 \n");
    printf("student id=%d\nstudent name=%s\nstudent date of birth=%d/%d/%d\nstudent CGPA=%f\nstudent fees=%f\n",s1.rno,s1.name,s1.d.dd,s1.d.mm,s1.d.yy,s1.cgpa,s1.fees);
    printf("\n\ndetails of the student 2\n");
    printf("student id=%d\nstudent name=%s\nstudent date of birth=%d/%d/%d\nstudent CGPA=%f\nstudent fees=%f\n",s2.rno,s2.name,s2.d.dd,s2.d.mm,s2.d.yy,s2.cgpa,s2.fees);
    if(s1.cgpa>s2.cgpa)
    printf("the cgpa of first student is more than second student ");
    else 
    printf("the cgpa of second student is more than first student");
    return 0;
}