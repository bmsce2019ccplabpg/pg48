#include <stdio.h>
int main()
{
    struct student
    {
        int rno;
        char name[20];
        struct dob
        {
            int dd;
            int mm;
            int yy;
        }d;
        float fees;
        float agg;
    }s;
    
    printf("Enter student details:");
    printf("\nRoll no.= ");
    scanf("%d",&s.rno);
    printf("\nName = ");
    scanf("%s",s.name);
    printf("\nEnter the date of birth as dd/mm/yy respectively= ");
    scanf("%d %d %d",&s.d.dd,&s.d.mm,&s.d.yy);
    printf("\nStudent fees= ");
    scanf("%f",&s.fees);
    printf("\nStudent aggregate= ");
    scanf("%f",&s.agg);
    
    printf("\nThe student details are as follows");
    printf("\nRoll no.=%d",s.rno);
    printf("\nName =%s",s.name);
    printf("\ndob=%d/%d/%d",s.d.dd,s.d.mm,s.d.yy);
    printf("\nStudent fees=%f",s.fees);
    printf("\nStudent aggregate=%f",s.agg);
    return 0;
}