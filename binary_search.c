#include <stdio.h> 
int input1();
void input2(int[],int);
int compute(int[],int);
void output(int);
int main()
{
    int n;
    n=input1();
    int a[n];
    input2(a,n);
    int pos;
    pos=compute(a,n);
    output(pos);
    return 0;
}
int input1()
{
    int n;
    printf("Enter the number of elements in an array");
    scanf("%d",&n);
    return n;
}
void input2(int a[],int n)
{
    int num;
    for(int i=0;i<n;i++)
    {
        printf("Enter a number: ");
        scanf("%d",&num);
        a[i]=num;
    }
}
int compute(int a[],int n)
{
    int val,pos;
    printf("Enter the number to be searched");
    scanf("%d",&val);
    int l=0,h=n-1;
    int m;
    while(l<=h)
    {
        pos=0;
        m=(l+h)/2;
        if(val==a[m])
        {
            pos=m+1;
            break;
        }
        else if(val>a[m])
            l=m+1;
        else
            h=m-1;
    }
    return pos;
}
void output(int pos)
{
    if(pos>0)
        printf("The given element was found at %d",pos);
    else
        printf("The given element was not found");
}


