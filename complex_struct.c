#include <stdio.h>
int main()
{
    struct complex
    {
        int real;
        int img;
    }c1,c2,sum;
    
    printf("Enter the real and imaginary part of number 1=");
    scanf("%d %d",&c1.real,&c1.img);
    printf("Enter the real and imaginary part of number 2=");
    scanf("%d %d",&c2.real,&c2.img);
    sum.real=c1.real+c2.real;
    sum.img=c1.img+c2.img;
    printf("The sum of complex number is %d+i%d",sum.real,sum.img);
    return 0;
}