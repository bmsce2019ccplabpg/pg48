#include <stdio.h> 
#include <math.h>
float input();
float compute(float,float,float,float);
void display(float);
int main()
{
    float x1,y1,x2,y2;
    float dist;
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    dist=compute(x1,y1,x2,y2);
    display(dist);
    return 0;
}
float input()
{
    float z;
    printf("Enter the coordinate of a point");
    scanf("%f",&z);
    return z;
}
float compute(float x1,float y1,float x2,float y2)
{
    float dist;
    dist=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return dist;
}
void display(float dist)
{
    printf("Distance between two given points=%f",dist);
}    
    