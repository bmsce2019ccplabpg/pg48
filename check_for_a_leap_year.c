#include <stdio.h> 
int input();
int compute(int);
void display(int);
int main()
{
    int year;
    int value;
    year=input();
    value=compute(year);
    display(value);
    return 0;
}
int input()
{
    int year;
    printf("Enter the year:");
    scanf("%d",&year);
    return year;
}
int compute(int year)
{
    if((year%4==0 && year%100!=0 )|| (year%400==0))
    {    
    return 1;
    }
    else
    {
    return 0;
    }
}
void display(int value)
{
    if(value==1)
    {
        printf("The given year is a leap year\n");
    }
    else
    {
    printf("The given year is not a leap year\n");
    }
}