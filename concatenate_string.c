#include <stdio.h> 
void input1(char[]);
void input2(char[]);
void compute(char[],char[],char[]);
void output(char[]);
int main()
{
    char s1[100];
    input1(s1);
    char s2[100];
    input2(s2);
    char s3[200];
    compute(s1,s2,s3);
    output(s3);
    return 0;
}
void input1(char s1[])
{
    printf("Enter the string");
    gets(s1);
}
void input2(char s2[])
{
    printf("Enter the string");
    gets(s2);
}
void compute(char s1[],char s2[],char s3[])
{
    int i=0,j=0;
    while(s1[i]!='\0')
    {
       s3[j]=s1[i];
       i++;
       j++; 
    }
    i=0;
    while(s2[i]!='\0')
    {
        s3[j]=s2[i];
        i++;
        j++;
    }
    s3[j]='\0';
}
void output(char s3[])
{
    printf("The concatenated string is %s",s3);
}