#include <stdio.h> 
int input();
void output(int);
int main()
{
    int n;
    n=input();
    output(n);
    return 0;
}
int input()
{
    int n;
    printf("Enter the value of number: ");
    scanf("%d",&n);
    return n;
}
void output(int n)
{
    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<=i;j++)
        {
            printf("%d ",i);
        }
        printf("\n");
    }
}
